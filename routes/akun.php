<?php
//akun
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Kcabang','Kagen']],function(){
        Route::group([
            'prefix' => 'akun','as' => 'akun.',
        ], function(){
            Route::get('/api','AkunController@api')->name('api');

            Route::get('/','AkunController@index')->name('akun');
            Route::get('/data_akun','AkunController@data_akun')->name('data_akun');
            Route::get('/json_akun','AkunController@json_akun')->name('json_akun');
            Route::post('/input_akun','AkunController@input_akun')->name('input_akun');
            Route::post('/input_akun_cabang/{id}','AkunController@input_akun_cabang')->name('input_akun_cabang');
            Route::post('/input_akun_agen/{id}','AkunController@input_akun_agen')->name('input_akun_agen');
            Route::post('/update_akun/{id}','AkunController@update_akun')->name('update_akun');
            Route::get('/delete_akun/{id}','AkunController@delete_akun')->name('delete_akun');

            Route::post('/input_karyawan_cabang','AkunController@input_karyawan_cabang')->name('input_karyawan_cabang');
            Route::post('/input_karyawan_agen','AkunController@input_karyawan_agen')->name('input_karyawan_agen');
            
            
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Kcabang','Kagen']],function(){
        Route::group([
            'prefix' => 'akun','as' => 'akun.',
        ], function(){

            Route::post('/input_karyawan_cabang','AkunController@input_karyawan_cabang')->name('input_karyawan_cabang');
            Route::post('/input_karyawan_agen','AkunController@input_karyawan_agen')->name('input_karyawan_agen');
            
            
        });
    });
});