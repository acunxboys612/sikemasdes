<?php
        Route::group(['middleware' => ['web','roles']],function(){
            Route::group(['roles'=>['Master','Kcabang','Keucab','Adcab','Karcab']],function(){
                Route::group([
                    'prefix' => 'price','as' => 'price.',
                ], function(){
                    Route::get('/','PriceController@index')->name('price');
                    //akun price
                    Route::get('/data_price','PriceController@data_price')->name('data_price');
                    Route::get('/json_price','PriceController@json_price')->name('json_price');
                    Route::post('/input_price','PriceController@input_price')->name('input_price');
                    Route::post('/update_price/{id}','PriceController@update_price')->name('update_price');
                    Route::get('/delete_price/{id}','PriceController@delete_price')->name('delete_price');
                    
                    Route::post('/import_price_list','PriceController@import_price_list')->name('import_price_list');
        
                });
            });
        });
        Route::group([
            'prefix' => 'price','as' => 'price.',
        ], function(){
            Route::get('/api_price/{id}','PriceController@api_price')->name('api_price');
            Route::get('/api_kota','PriceController@api_kota')->name('api_kota');
            Route::get('/cek_tarif','PriceController@cek_tarif')->name('cek_tarif');
            Route::get('/cek_tarif_admin','PriceController@cek_tarif_admin')->name('cek_tarif_admin');
        });