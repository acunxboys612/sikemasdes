<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Ceo']],function(){
        Route::group([
            'prefix' => 'perusahaan','as' => 'perusahaan.',
        ], function(){
            Route::get('/api','CabangController@api')->name('api');
            Route::get('/data_perusahaan','PerusahaanController@data_perusahaan')->name('data_perusahaan');
            Route::get('/json_perusahaan','PerusahaanController@json_perusahaan')->name('json_perusahaan');
            Route::post('/input_perusahaan','PerusahaanController@input_perusahaan')->name('input_perusahaan');
            Route::post('/update_perusahaan/{id}','PerusahaanController@update_perusahaan')->name('update_perusahaan');
            Route::get('/delete_perusahaan/{id}','PerusahaanController@delete_perusahaan')->name('delete_perusahaan');
            
        });
    });
});