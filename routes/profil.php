<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Admin','Keuangan','Karyawan','Ceo','Keucab','Adcab','Keuagen','Adagen','Karcab','Karagen','Kcabang','Kagen']],function(){
        Route::group([
            'prefix' => 'profil','as' => 'profil.',
        ], function(){
            //akun
            Route::get('/data_profil_karyawan','ProfilController@data_profil_karyawan')->name('data_profil_karyawan');
            Route::get('/data_profil','ProfilController@data_profil')->name('data_profil');
            Route::post('/update_profil','ProfilController@update_profil')->name('update_profil');
            Route::post('/update_foto_profil','ProfilController@update_foto_profil')->name('update_foto_profil');

            Route::get('/data_password','ProfilController@data_password')->name('data_password');
            Route::post('/update_password','ProfilController@update_password')->name('update_password');
            Route::post('/update_password_karyawan/{id}','ProfilController@update_password_karyawan')->name('update_password_karyawan');

        });
    });
});