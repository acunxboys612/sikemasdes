<?php
//backup
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'backup','as' => 'backup.',
        ], function(){
            Route::get('/backupdb','BackupController@backupdb')->name('backupdb');
            Route::get('/data_backup','BackupController@data_backup')->name('data_backup');
            Route::get('/json_backup','BackupController@json_backup')->name('json_backup');
            Route::post('/input_backup','BackupController@input_backup')->name('input_backup');
            Route::post('/update_backup/{id}','BackupController@update_backup')->name('update_backup');
            Route::get('/delete_backup/{id}','BackupController@delete_backup')->name('delete_backup');
            Route::get('/backup', function () {
                \Illuminate\Support\Facades\Artisan::call('backup:run --only-db');
                return 'Successful backup!';
            })->name('backup');
        });
    });
});