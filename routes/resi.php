<?php
Route::group([
    'prefix' => 'resi','as' => 'resi.',
], function(){
    // Route::get('/{id}','ResiController@index')->name('index');
    Route::get('cekresi/','ResiController@cekresi')->name('cekresi');
    Route::get('/{resi}','ResiController@resi')->name('resi');
    Route::get('/cetak_resi/{id}','ResiController@cetak_resi')->name('cetak_resi');
});