<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Ceo','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'cabang','as' => 'cabang.',
        ], function(){
            Route::get('/api','CabangController@api')->name('api');
            Route::get('/data_cabang','CabangController@data_cabang')->name('data_cabang');
            Route::get('/json_cabang','CabangController@json_cabang')->name('json_cabang');
            Route::get('/data_karcab/{id}','CabangController@data_karcab')->name('data_karcab');
            Route::get('/json_karcab/{id}','CabangController@json_karcab')->name('json_karcab');
            Route::post('/input_cabang','CabangController@input_cabang')->name('input_cabang');
            Route::post('/update_cabang/{id}','CabangController@update_cabang')->name('update_cabang');
            Route::get('/delete_cabang/{id}','CabangController@delete_cabang')->name('delete_cabang');

            Route::get('/data_karyawan_cabang','CabangController@data_karyawan_cabang')->name('data_karyawan_cabang');
            Route::get('/json_karyawan_cabang','CabangController@json_karyawan_cabang')->name('json_karyawan_cabang');
            
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Kcabang']],function(){
        Route::group([
            'prefix' => 'cabang','as' => 'cabang.',
        ], function(){
            Route::get('/data_karyawan_cabang','CabangController@data_karyawan_cabang')->name('data_karyawan_cabang');
            Route::get('/json_karyawan_cabang','CabangController@json_karyawan_cabang')->name('json_karyawan_cabang');
            
        });
    });
});