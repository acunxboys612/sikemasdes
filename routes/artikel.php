<?php
//untuk SEO
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Karcab','Karagen']],function(){
        Route::group([
            'prefix' => 'artikel','as' => 'artikel.',
        ], function(){
            
            Route::get('/form_artikel','ArtikelController@form_artikel')->name('form_artikel');
            Route::get('/form_update_post/{id}','ArtikelController@form_update_post')->name('form_update_post');
            Route::get('/data_artikel_all','ArtikelController@data_artikel_all')->name('data_artikel_all');
            Route::get('/json_artikel_all','ArtikelController@json_artikel_all')->name('json_artikel_all');
            Route::post('/input_post','ArtikelController@input_post')->name('input_post');
            Route::post('/update_post/{id}','ArtikelController@update_post')->name('update_post');
            Route::get('/delete_post/{id}','ArtikelController@delete_post')->name('delete_post');

            Route::get('/data_post_kategori','ArtikelController@data_post_kategori')->name('data_post_kategori');
            Route::get('/json_post_kategori','ArtikelController@json_post_kategori')->name('json_post_kategori');
            Route::post('/input_post_kategori','ArtikelController@input_post_kategori')->name('input_post_kategori');
            Route::post('/update_post_kategori/{id}','ArtikelController@update_post_kategori')->name('update_post_kategori');
            Route::get('/delete_post_kategori/{id}','ArtikelController@delete_post_kategori')->name('delete_post_kategori');

            Route::get('/data_faq/{id}','ArtikelController@data_faq')->name('data_faq');
            Route::get('/json_faq/{id}','ArtikelController@json_faq')->name('json_faq');
            Route::post('/input_faq','ArtikelController@input_faq')->name('input_faq');
            Route::post('/update_faq/{id}','ArtikelController@update_faq')->name('update_faq');
            Route::get('/delete_faq/{id}','ArtikelController@delete_faq')->name('delete_faq');
            
        });
    });
});
//Untuk Penulis
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'artikel','as' => 'artikel.',
        ], function(){
            
            Route::get('/data_artikel/{id}','ArtikelController@data_artikel')->name('data_artikel');
            Route::get('/json_artikel/{id}','ArtikelController@json_artikel')->name('json_artikel');
            Route::post('/input_artikel','ArtikelController@input_artikel')->name('input_artikel');
            Route::post('/update_arikel/{id}','ArtikelController@update_arikel')->name('update_arikel');
            Route::get('/delete_artikel/{id}','ArtikelController@delete_artikel')->name('delete_artikel');
            
        });
    });
});