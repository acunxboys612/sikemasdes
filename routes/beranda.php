<?php
//beranda
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/master','BerandaController@master')->name('master');
            Route::get('/ceo','BerandaController@ceo')->name('ceo');
            Route::get('/keungan','BerandaController@keungan')->name('keungan');
            Route::get('/admin','BerandaController@admin')->name('admin');
            Route::get('/keucab','BerandaController@keucab')->name('keucab');
            Route::get('/adcab','BerandaController@adcab')->name('adcab');
            Route::get('/keuagen','BerandaController@keuagen')->name('keuagen');
            Route::get('/adagen','BerandaController@adagen')->name('adagen');
            Route::get('/karyawan','BerandaController@karyawan')->name('karyawan');
            Route::get('/karcab','BerandaController@karcab')->name('karcab');
            Route::get('/kargen','BerandaController@kargen')->name('kargen');
            Route::get('/kcabang','BerandaController@kcabang')->name('kcabang');
            Route::get('/kagen','BerandaController@kagen')->name('kagen');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Ceo']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/ceo','BerandaController@ceo')->name('ceo');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Keuangan']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/keuangan','BerandaController@keuangan')->name('keuangan');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Admin']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/admin','BerandaController@admin')->name('admin');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Keucab']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/keucab','BerandaController@keucab')->name('keucab');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Adcab']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/adcab','BerandaController@adcab')->name('adcab');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Keuagen']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/keuagen','BerandaController@keuagen')->name('keuagen');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Adagen']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/adagen','BerandaController@adagen')->name('adagen');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Karyawan']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/karyawan','BerandaController@karyawan')->name('karyawan');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Karcab']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/karcab','BerandaController@karcab')->name('karcab');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Karagen']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/kargen','BerandaController@kargen')->name('kargen');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Kcabang']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/kcabang','BerandaController@kcabang')->name('kcabang');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Kagen']],function(){
        Route::group([
            'prefix' => 'beranda','as' => 'beranda.',
        ], function(){
            Route::get('/kagen','BerandaController@kagen')->name('kagen');
        });
    });
});
