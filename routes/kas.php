<?php

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Keucab','Kcabang','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'cabang','as' => 'cabang.',
        ], function(){
            Route::get('/api_kc_in','KascabController@api_kc_in')->name('api_kc_in');
            Route::get('/api_kc_out','KascabController@api_kc_out')->name('api_kc_out');
            Route::get('/data_kc','KascabController@data_kc')->name('data_kc');
            Route::get('/json_kc','KascabController@json_kc')->name('json_kc');
            Route::post('/input_kc','KascabController@input_kc')->name('input_kc');
            Route::post('/update_kc/{id}','KascabController@update_kc')->name('update_kc');
            Route::get('/delete_kc/{id}','KascabController@delete_kc')->name('delete_kc');

            Route::get('/data_kascab','KascabController@data_kascab')->name('data_kascab');
            Route::get('/json_kascab','KascabController@json_kascab')->name('json_kascab');
            Route::post('/input_kascab_in','KascabController@input_kascab_in')->name('input_kascab_in');
            Route::post('/input_kascab_out','KascabController@input_kascab_out')->name('input_kascab_out');
            Route::post('/update_kascab/{id}','KascabController@update_kascab')->name('update_kascab');
            Route::get('/delete_kascab/{id}','KascabController@delete_kascab')->name('delete_kascab');

            Route::get('/form_kc_in','KascabController@form_kc_in')->name('form_kc_in');
            Route::get('/form_kc_out','KascabController@form_kc_out')->name('form_kc_out');

            Route::get('/data_kas_cabang','KascabController@data_kas_cabang')->name('data_kas_cabang');
            Route::get('/json_kas_cabang','KascabController@json_kas_cabang')->name('json_kas_cabang');

            Route::get('/cetak_kascab','KascabController@cetak_kascab')->name('cetak_kascab');
            Route::get('/cetak_kas_cabang','KascabController@cetak_kas_cabang')->name('cetak_kas_cabang');

        });
    });
});

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Keuagen','Kagen','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'agen','as' => 'agen.',
        ], function(){
            Route::get('/api_ka_in','KasgenController@api_ka_in')->name('api_ka_in');
            Route::get('/api_ka_out','KasgenController@api_ka_out')->name('api_ka_out');
            Route::get('/data_ka','KasgenController@data_ka')->name('data_ka');
            Route::get('/json_ka','KasgenController@json_ka')->name('json_ka');
            Route::post('/input_ka','KasgenController@input_ka')->name('input_ka');
            Route::post('/update_ka/{id}','KasgenController@update_ka')->name('update_ka');
            Route::get('/delete_ka/{id}','KasgenController@delete_ka')->name('delete_ka');

            Route::get('/data_kasgen','KasgenController@data_kasgen')->name('data_kasgen');
            Route::get('/json_kasgen','KasgenController@json_kasgen')->name('json_kasgen');
            Route::post('/input_kasgen_in','KasgenController@input_kasgen_in')->name('input_kasgen_in');
            Route::post('/input_kasgen_out','KasgenController@input_kasgen_out')->name('input_kasgen_out');
            Route::post('/update_kasgen/{id}','KasgenController@update_kasgen')->name('update_kasgen');
            Route::get('/delete_kasgen/{id}','KasgenController@delete_kasgen')->name('delete_kasgen');

            Route::get('/form_ka_in','KasgenController@form_ka_in')->name('form_ka_in');
            Route::get('/form_ka_out','KasgenController@form_ka_out')->name('form_ka_out');

            Route::get('/data_kas_agen','KasgenController@data_kas_agen')->name('data_kas_agen');
            Route::get('/json_kas_agen','KasgenController@json_kas_agen')->name('json_kas_agen');

            Route::get('/cetak_kasgen','KasgenController@cetak_kasgen')->name('cetak_kasgen');
            Route::get('/cetak_kas_agen','KasgenController@cetak_kas_agen')->name('cetak_kas_agen');

        });
    });
});