<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});

Route::get('/login', function () {
    return view('login');
});
Route::get('login', function () {
    return view('login');
});
Route::get('/enc/{id}','BerandaController@enc')->name('enc');
include('resi.php');
include('akun.php');
include('beranda.php');
include('perusahaan.php');
include('cabang.php');
include('agen.php');
include('barang.php');
include('crowl.php');
include('dm.php');
include('kas.php');
include('home.php');
include('artikel.php');
include('price.php');
include('profil.php');
include('layanan.php');
Route::post('masuk', 'LoginController@masuk')->name('masuk');
Route::get('keluar', 'LoginController@keluar')->name('keluar');
Route::get('kupon/{kode}', 'KuponController@kupon')->name('kupon');
Route::get('sosmed', 'KuponController@sosmed')->name('sosmed');
Route::get('has/{id}', 'LoginController@has')->name('has');
Route::get('id/search/produk/{jasa}/{id}', 'CrowlController@cek')->name('cek');

