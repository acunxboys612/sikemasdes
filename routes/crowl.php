<?php
        Route::group([
            'prefix' => 'crowl','as' => 'crowl.',
        ], function(){
            Route::get('/lkpp','CrowlController@lkpp')->name('lkpp');
            Route::get('/lkppv/{id}','CrowlController@lkppv')->name('lkppv');
            Route::get('/lkppkat/{id}','CrowlController@lkppkat')->name('lkppkat');
        });
