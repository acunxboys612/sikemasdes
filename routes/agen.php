<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Ceo','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'agen','as' => 'agen.',
        ], function(){
            Route::get('/api','AgenController@api')->name('api');
            Route::get('/data_agen','AgenController@data_agen')->name('data_agen');
            Route::get('/json_agen','AgenController@json_agen')->name('json_agen');
            Route::get('/data_karagen/{id}','AgenController@data_karagen')->name('data_karagen');
            Route::get('/json_karagen/{id}','AgenController@json_karagen')->name('json_karagen');
            Route::post('/input_agen','AgenController@input_agen')->name('input_agen');
            Route::post('/update_agen/{id}','AgenController@update_agen')->name('update_agen');
            Route::get('/delete_agen/{id}','AgenController@delete_agen')->name('delete_agen');

            Route::get('/data_karyawan_agen','AgenController@data_karyawan_agen')->name('data_karyawan_agen');
            Route::get('/json_karyawan_agen','AgenController@json_karyawan_agen')->name('json_karyawan_agen');
            
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Kagen']],function(){
        Route::group([
            'prefix' => 'agen','as' => 'agen.',
        ], function(){

            Route::get('/data_karyawan_agen','AgenController@data_karyawan_agen')->name('data_karyawan_agen');
            Route::get('/json_karyawan_agen','AgenController@json_karyawan_agen')->name('json_karyawan_agen');
            
        });
    });
});