<?php
//Barang
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'barang','as' => 'barang.',
        ], function(){
            Route::get('/data_barang','BarangController@data_barang')->name('data_barang');
            Route::get('/json_barang','BarangController@json_barang')->name('json_barang');
            Route::get('/delete_barang/{id}','BarangController@delete_barang')->name('delete_barang');
            Route::get('/cetak_barang','BarangController@cetak_barang')->name('cetak_barang');
            Route::get('/cetak_barang_agen','BarangController@cetak_barang_agen')->name('cetak_barang_agen');
            Route::get('/cetak_barang_cabang','BarangController@cetak_barang_cabang')->name('cetak_barang_cabang');

        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Kcabang','Kagen','Adcab','Adagen','Keucab','Keuagen','Karcab','Karagen','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'barang','as' => 'barang.',
        ], function(){
            Route::get('/data_log_barang/{id}','BarangController@data_log_barang')->name('data_log_barang');
            Route::get('/json_log_barang/{id}','BarangController@json_log_barang')->name('json_log_barang');
            Route::post('/input_log_barang','BarangController@input_log_barang')->name('input_log_barang');
            Route::get('/update_log_barang/{id}','BarangController@update_log_barang')->name('update_log_barang');
            Route::get('/delete_log_barang/{id}','BarangController@delete_log_barang')->name('delete_log_barang');

            Route::get('/cetak_marking/{id}','BarangController@cetak_marking')->name('cetak_marking');

        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Keucab','Adcab','Kcabang','Karcab','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'barang','as' => 'barang.',
        ], function(){
            Route::get('/form_bc','BcController@form_bc')->name('form_bc');
            Route::get('/data_bc','BcController@data_bc')->name('data_bc');
            Route::get('/json_bc','BcController@json_bc')->name('json_bc');
            Route::post('/input_bc','BcController@input_bc')->name('input_bc');
            Route::get('/form_update_bc/{id}','BcController@form_update_bc')->name('form_update_bc');
            Route::post('/update_bc/{id}','BcController@update_bc')->name('update_bc');
            Route::get('/delete_bc/{id}','BcController@delete_bc')->name('delete_bc');
            Route::get('/status_pembayaran_bc/{id}','BcController@status_pembayaran_bc')->name('status_pembayaran_bc');
            Route::get('/status_stt_bc/{id}','BcController@status_stt_bc')->name('status_stt_bc');
            Route::get('/status_barang_bc/{id}','BcController@status_barang_bc')->name('status_barang_bc');
            Route::get('/cetak_resi_bc/{id}','BcController@cetak_resi_bc')->name('cetak_resi_bc');
            Route::get('/cetak_marking_bc/{id}','BcController@cetak_marking_bc')->name('cetak_marking_bc');

            Route::get('/form_terima_bc','BarangController@form_terima_bc')->name('form_terima_bc');
            Route::post('/input_terima_bc','BarangController@input_terima_bc')->name('input_terima_bc');

            Route::get('/cetak_barang_bc','BcController@cetak_barang_bc')->name('cetak_barang_bc');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Keuagen','Adagen','Kagen','Karagen','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'barang','as' => 'barang.',
        ], function(){
            Route::get('/form_ba','BaController@form_ba')->name('form_ba');
            Route::get('/data_ba','BaController@data_ba')->name('data_ba');
            Route::get('/json_ba','BaController@json_ba')->name('json_ba');
            Route::post('/input_ba','BaController@input_ba')->name('input_ba');
            Route::get('/form_update_ba/{id}','BaController@form_update_ba')->name('form_update_ba');
            Route::post('/update_ba/{id}','BaController@update_ba')->name('update_ba');
            Route::get('/delete_ba/{id}','BaController@delete_ba')->name('delete_ba');
            Route::get('/status_pembayaran_ba/{id}','BaController@status_pembayaran_ba')->name('status_pembayaran_ba');
            Route::get('/status_stt_ba/{id}','BaController@status_stt_ba')->name('status_stt_ba');
            Route::get('/status_barang_ba/{id}','BaController@status_barang_ba')->name('status_barang_ba');
            Route::get('/cetak_resi_ba/{id}','BaController@cetak_resi_ba')->name('cetak_resi_ba');
            Route::get('/cetak_marking_ba/{id}','BaController@cetak_marking_ba')->name('cetak_marking_ba');

            Route::get('/form_terima_ba','BarangController@form_terima_ba')->name('form_terima_ba');
            Route::post('/input_terima_ba','BarangController@input_terima_ba')->name('input_terima_ba');

            Route::get('/cetak_barang_ba','BaController@cetak_barang_ba')->name('cetak_barang_ba');
        });
    });
});