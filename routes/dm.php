<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Keucab','Adcab','Kcabang','Karcab','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'cabang', 'as'=> 'cabang.',
        ], function(){

            Route::get('/data_dc','DcController@data_dc')->name('data_dc');
            Route::get('/json_dc','DcController@json_dc')->name('json_dc');
            Route::post('/input_dc','DcController@input_dc')->name('input_dc');
            Route::post('/update_dc/{id}','DcController@update_dc')->name('update_dc');
            Route::get('/delete_dc/{id}','DcController@delete_dc')->name('delete_dc');

            Route::get('/data_dc_all','DcController@data_dc_all')->name('data_dc_all');
            Route::get('/json_dc_all','DcController@json_dc_all')->name('json_dc_all');

            Route::get('/data_ceklist_dc/{id}','DcController@data_ceklist_dc')->name('data_ceklist_dc');
            Route::get('/json_ceklist_dc','DcController@json_ceklist_dc')->name('json_ceklist_dc');
            Route::post('/input_ceklist_dc/{id}','DcController@input_ceklist_dc')->name('input_ceklist_dc');

            Route::get('/data_barang_dc/{id}','DcController@data_barang_dc')->name('data_barang_dc');
            Route::get('/json_barang_dc/{id}','DcController@json_barang_dc')->name('json_barang_dc');
            Route::get('/delete_barang_dc/{id}','DcController@delete_barang_dc')->name('delete_barang_dc');

            Route::get('/form_dc_transit','DcController@form_dc_transit')->name('form_dc_transit');
            Route::post('/input_dc_transit','DcController@input_dc_transit')->name('input_dc_transit');

            Route::get('/cetak_dc/{id}','DcController@cetak_dc')->name('cetak_dc');

        });
    });
});

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Keuagen','Adagen','Kagen','Karagen','Admin','Keuangan']],function(){
        Route::group([
            'prefix' => 'agen', 'as'=> 'agen.',
        ], function(){

            Route::get('/data_da','DaController@data_da')->name('data_da');
            Route::get('/json_da','DaController@json_da')->name('json_da');
            Route::post('/input_da','DaController@input_da')->name('input_da');
            Route::post('/update_da/{id}','DaController@update_da')->name('update_da');
            Route::get('/delete_da/{id}','DaController@delete_da')->name('delete_da');

            Route::get('/data_da_all','DaController@data_da_all')->name('data_da_all');
            Route::get('/json_da_all','DaController@json_da_all')->name('json_da_all');

            Route::get('/data_ceklist_da/{id}','DaController@data_ceklist_da')->name('data_ceklist_da');
            Route::get('/json_ceklist_da','DaController@json_ceklist_da')->name('json_ceklist_da');
            Route::post('/input_ceklist_da/{id}','DaController@input_ceklist_da')->name('input_ceklist_da');

            Route::get('/data_barang_da/{id}','DaController@data_barang_da')->name('data_barang_da');
            Route::get('/json_barang_da/{id}','DaController@json_barang_da')->name('json_barang_da');
            Route::get('/delete_barang_da/{id}','DaController@delete_barang_da')->name('delete_barang_da');

            Route::get('/form_da_transit','DaController@form_da_transit')->name('form_da_transit');
            Route::post('/input_da_transit','DaController@input_da_transit')->name('input_da_transit');

            Route::get('/cetak_da/{id}','DaController@cetak_da')->name('cetak_da');

        });
    });
});