<?php

    Route::get('/','HomepageController@index')->name('homepage');
    Route::get('/blog','HomepageController@blog')->name('blog');
    Route::get('/sejarah','HomepageController@sejarah')->name('sejarah');
    Route::get('/visimisi','HomepageController@visimisi')->name('visimisi');
    Route::get('/geografis','HomepageController@geografis')->name('geografis');
    Route::get('/inovasi','HomepageController@inovasi')->name('inovasi');
    Route::get('/galeri','HomepageController@galeri')->name('galeri');
    Route::get('/kontak','HomepageController@kontak')->name('kontak');
    
    Route::get('/populer','HomepageController@populer')->name('populer');
    Route::get('/kategori/{slug}','HomepageController@kategori')->name('kategori');
    Route::get('/tag/{slug}','HomepageController@tag')->name('tag');
    Route::get('/login','HomepageController@login')->name('login');
    Route::get('/404','HomepageController@notfound')->name('notfound');
    Route::get('/sitemap.xml','HomepageController@sitemap')->name('sitemap');
    
    Route::post('masuk', 'LoginController@masuk')->name('masuk');
    Route::post('bypass', 'LoginController@bypass')->name('bypass');
    Route::get('keluar', 'LoginController@keluar')->name('keluar');
    Route::get('booking', 'BarangController@booking')->name('booking');
    Route::get('/{slug}','HomepageController@post')->name('post');
    Route::post('/input_booking','BarangController@input_booking')->name('input_booking');


    Route::get('/kirim_email','MailController@kirim_email')->name('kirim_email');

    Route::get('site/sitemap.xml', 'HomepageController@postmap')->name('postmap');

