<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Karcab','Karagen']],function(){
        Route::group([
            'prefix' => 'layanan','as' => 'layanan.',
        ], function(){
            Route::get('/','LayananController@layanan')->name('layanan');
            Route::get('/api','LayananController@api')->name('api');
            Route::get('/data_layanan','LayananController@data_layanan')->name('data_layanan');
            Route::get('/json_layanan','LayananController@json_layanan')->name('json_layanan');
            Route::post('/input_layanan','LayananController@input_layanan')->name('input_layanan');
            Route::post('/update_layanan/{id}','LayananController@update_layanan')->name('update_layanan');
            Route::get('/delete_layanan/{id}','LayananController@delete_layanan')->name('delete_layanan');
        });
    });
});

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Karcab','Karagen']],function(){
        Route::group([
            'prefix' => 'slider','as' => 'slider.',
        ], function(){
            Route::get('/','LayananController@slider')->name('slider');
            Route::get('/api','LayananController@api')->name('api');
            Route::get('/data_slider','LayananController@data_slider')->name('data_slider');
            Route::get('/json_slider','LayananController@json_slider')->name('json_slider');
            Route::post('/input_slider','LayananController@input_slider')->name('input_slider');
            Route::post('/update_slider/{id}','LayananController@update_slider')->name('update_slider');
            Route::get('/delete_slider/{id}','LayananController@delete_slider')->name('delete_slider');
        });
    });
});