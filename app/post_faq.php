<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post_faq extends Model
{
    protected $fillable = [
        'tanya',
        'jawab',
        'post_id',
    ];
    public function post()
    {
        return $this->belongsTo('App\post','post_id');
    }
}
