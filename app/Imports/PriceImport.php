<?php

namespace App\Imports;

use App\price_list;
use Maatwebsite\Excel\Concerns\ToModel;

class PriceImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    protected $id;

    function __construct($id) {
    $this->id = $id;
    }
    public function model(array $row)
    {
        if ($row['0']!=null||$row['0']!='No') {
            $cek = price_list::where('kota_id','=',$this->id)->where('kota','=',$row['0'])->count();
            if ($cek==0) {
                price_list::create([
                    'kota_id'=>$this->id,
                    'kota'=>$row['0'],
                    'min_kg'=>$row['1'],
                    'harga_kg'=>$row['2'],
                    'harga_kubik'=>$row['3'],
                    'estimasi'=>$row['4'],
                ]);
            }else{

            }
         }
    }
}
