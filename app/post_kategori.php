<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post_kategori extends Model
{
    protected $fillable = [
        'judul',
        'slug',
        'deskripsi',
    ];

    public function post_kategori_log()//menampilkan user dengan data tes yang diikuti
    {
        return $this->hasMany('App\post_kategori_log');
    }
}
