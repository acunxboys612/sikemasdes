<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post_seo extends Model
{
    protected $fillable = [
        'title',
        'deskripsi',
        'keyword',
        'post_id',
    ];
    public function post()
    {
        return $this->belongsTo('App\post','post_id');
    }
}
