<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    protected $fillable = [
        'nama',
        'pic',
        'posisi',
        'status',
        'alt',
        'text',
    ];
}
