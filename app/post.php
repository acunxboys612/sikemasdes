<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $fillable = [
        'judul',
        'slug',
        'deskripsi',
        'isi',
        'tag',
        'img',
        'alt',
        'status',
        'user_id',
    ];
    public function post_faq()
    {
        return $this->hasMany('App\post_faq');
    }
    public function post_seo()
    {
        return $this->hasMany('App\post_seo');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function post_kategori_log()
    {
        return $this->hasMany('App\post_kategori_log');
    }
}
