<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post_kategori_log extends Model
{
    protected $fillable = [
        'post_id',
        'post_kategori_id',
    ];
    public function post()
    {
        return $this->belongsTo('App\post','post_id');
    }
    public function post_kategori()
    {
        return $this->belongsTo('App\post_kategori','post_kategori_id');
    }
}
