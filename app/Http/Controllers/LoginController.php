<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{
    public function masuk(Request $request)
    {
        $request->validate([
            'username'=>['required'],
            'password'=>['required']
          ]);
         $username=$request->get('username');
         $password=$request->get('password');
         $user_cek=User::where('username','=',$username)->count();
         if ($user_cek==0) {
            return redirect('/login')->with('gagal', 'username Anda Tidak Terdaftar');
         }else{
            $user=User::where('username','=',$username)->first();
            $p_asli=$password;
            $p_hash=$user->password;
            $cek=Hash::check($p_asli, $p_hash);
                if ($cek) {
                    Auth::guard('web')->loginUsingId($user->id);
                    if ($user->role_id==1) {
                        return redirect('beranda/master')->with('success', 'Selamat Datang Master');
                    }else if ($user->role_id==2) {
                        return redirect('beranda/ceo')->with('success', 'Selamat Datang CEO');
                    }else if ($user->role_id==3) {
                        return redirect('beranda/keuangan')->with('success', 'Selamat Datang Bagian Keungan');
                    }else if ($user->role_id==4) {
                        return redirect('beranda/admin')->with('success', 'Selamat Datang Admin');
                    }else if ($user->role_id==5) {
                        return redirect('beranda/keucab')->with('success', 'Selamat Datang');
                    }else if ($user->role_id==6) {
                        return redirect('beranda/adcab')->with('success', 'Selamat Datang');
                    }else if ($user->role_id==7) {
                        return redirect('beranda/keuagen')->with('success', 'Selamat Datang');
                    }else if ($user->role_id==8) {
                        return redirect('beranda/adagen')->with('success', 'Selamat Datang');
                    }else if ($user->role_id==9) {
                        return redirect('beranda/karyawan')->with('success', 'Selamat Datang');
                    }else if ($user->role_id==10) {
                        return redirect('beranda/karcab')->with('success', 'Selamat Datang');
                    }else if ($user->role_id==11) {
                        return redirect('beranda/kargen')->with('success', 'Selamat Datang');
                    }else if ($user->role_id==12) {
                        return redirect('beranda/kcabang')->with('success', 'Selamat Datang');
                    }else if ($user->role_id==13) {
                        return redirect('beranda/kagen')->with('success', 'Selamat Datang');
                    }else{
                        return redirect('/login')->with('gagal', 'Anda Buka Siapa Siapa!');
                    }
                    }else{
                        return redirect('/login')->with('gagal', 'Password Anda Salah');
                }
         }
    }
    public function keluar()
    {
        if (Auth::guard('web')->check()) {
            Auth::guard('web','roles')->logout();
            return redirect('/login');
        }else{
            return redirect('/login');
        }
    }
    public function has($id)
    {
        $cek=Hash::make($id);
        echo $cek;
    }
}
