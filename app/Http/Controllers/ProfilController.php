<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\cabang;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class ProfilController extends Controller
{
    public function data_profil_karyawan()
    {
        $user=Auth::User()->id;
        $data=User::where('id','=',$user)->with(['role'])->get();
        //dd($data->toArray());
        return view('profil.data_profil_karyawan',compact('data'));
    }
    public function data_profil()
    {
        return view('profil.data_profil');
    }
    public function update_profil(Request $request)
        {
            $user_id=Auth::User()->id;
            $data=User::find($user_id);
            $data->name=$request->get('name');
            $data->jk=$request->get('jk');
            $data->hp=$request->get('hp');
            $data->save();
            return redirect()->back()->with('success', 'Profil Berhasil Update');
        }
        public function update_foto_profil(Request $request)
        {
            $request->validate([
                'pic' => ['image','mimes:jpeg,png,jpg,gif,svg']
              ]);
              $user_id=Auth::User()->id;
                $data=User::find($user_id);
                $foto = $request->file('pic');
                $path = $foto->store('public/profil');
                Storage::delete($data->pic);
                $data->pic=$path;
                $data->save();
            return redirect()->back()->with('success', 'Berhasil Diperbaharui');
        }
            public function data_password()
            {
                return view('profil.reset_password');
            }
            public function update_password(Request $request)
                {
                $user_id=Auth::User()->id;
                $data=User::find($user_id);
                $data->password=$request->get('password');
                $data->save();
                return redirect()->back()->with('success', 'Password Berhasil Update');
            }
            public function update_password_karyawan(Request $request,$id)
            {
                $data=User::find($id);
                $data->password=$request->get('password');
                $data->save();
                return redirect()->back()->with('success', 'Password Berhasil Diubah');
            }
}
