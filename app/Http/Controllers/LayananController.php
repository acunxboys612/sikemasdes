<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\layanan;
use App\slider;
use App\log;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class LayananController extends Controller
{
    public function data_layanan()
    {
        return view('layanan.data_layanan');
    }
    public function json_layanan()
    {
    $data=layanan::all();
    //dd($data->toArray());
    return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary">Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('layanan.update_layanan',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-6">
                                     <label><b>Nama Layanan*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="nama_layanan" placeholder="Nama Layanan" value="'.$user->nama_layanan.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Deskripsi*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi" required value="'.$user->deskripsi.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Foto Layanan*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="file" class="form-control" name="pic">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 </div>
                             <div class="row">
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning">Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('layanan.delete_layanan',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->addColumn('pic', function ($user) {
                    $pic=url('/').Storage::url($user->pic);
                    return'
                    <img src="'.$pic.'" alt="" width="100">
                    ';
                })
                ->escapeColumns([])
                ->make(true);
}

public function input_layanan(Request $request)
{
    $request->validate([
        'nama_layanan'=>['required'],
        'deskripsi'=>['required'],
        ]);
        $foto = $request->file('pic');
        $path = $foto->store('public/layanan');
        $user_id = layanan::insertGetId([
            'nama_layanan' => $request->get('nama_layanan'),
            'deskripsi' => $request->get('deskripsi'), 
            'pic' => $path, 
    ]);
    return redirect()->back()->with('success', 'Layanan Berhasil Dibuat');
}
public function update_layanan(Request $request, $id)
    {
        $foto = $request->file('pic');
        if ($foto==null) {
            $data=layanan::find($id);
            $data->nama_layanan=$request->get('nama_layanan');
            $data->deskripsi=$request->get('deskripsi');
            $pic=$data->pic;
            $data->save();
            return redirect()->back()->with('success', 'Akun Berhasil Update');
        }else{
            $path = $foto->store('public/layanan');
            $data=layanan::find($id);
            $data->nama_layanan=$request->get('nama_layanan');
            $data->deskripsi=$request->get('deskripsi');
            $data->pic=$path;
            $data->save();
            return redirect()->back()->with('success', 'Akun Berhasil Update');
        }
    }
    public function delete_layanan($id)
    {
        $data=layanan::find($id);
        Storage::delete($data->pic);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
    //SLIDER
    public function data_slider()
    {
        return view('layanan.data_slider');
    }
    public function json_slider()
    {
    $data=slider::all();
    //dd($data->toArray());
    return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary">Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('slider.update_slider',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-6">
                                     <label><b>Nama*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="nama" placeholder="Nama" value="'.$user->nama.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Posisi*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="posisi" placeholder="Posisi" required value="'.$user->posisi.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Status*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="status" placeholder="Status" required value="'.$user->status.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Alt*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="alt" placeholder="Alt" required value="'.$user->alt.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Text*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="text" placeholder="Text" required value="'.$user->text.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Foto *</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="file" class="form-control" name="pic">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 </div>
                             <div class="row">
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning">Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('slider.delete_slider',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->addColumn('pic', function ($user) {
                    $pic=url('/').Storage::url($user->pic);
                    return'
                    <img src="'.$pic.'" alt="" width="100">
                    ';
                })
                ->escapeColumns([])
                ->make(true);
}

public function input_slider(Request $request)
{
    $request->validate([
        'nama'=>['required'],
        'posisi'=>['required'],
        'alt'=>['required'],
        ]);
        $foto = $request->file('pic');
        $path = $foto->store('public/slider');
        $user_id = slider::insertGetId([
            'nama' => $request->get('nama'),
            'posisi' => $request->get('posisi'), 
            'alt' => $request->get('alt'), 
            'text' => $request->get('text'), 
            'status' => "NONAKTIF", 
            'pic' => $path, 
    ]);
    return redirect()->back()->with('success', 'Layanan Berhasil Dibuat');
}
public function update_slider(Request $request, $id)
    {
        $foto = $request->file('pic');
        if ($foto==null) {
            $data=slider::find($id);
            $data->nama=$request->get('nama');
            $data->posisi=$request->get('posisi');
            $data->alt=$request->get('alt');
            $data->text=$request->get('text');
            $pic=$data->pic;
            $data->save();
            return redirect()->back()->with('success', 'Akun Berhasil Update');
        }else{
            $path = $foto->store('public/slider');
            $data=slider::find($id);
            $data->nama=$request->get('nama');
            $data->posisi=$request->get('posisi');
            $data->alt=$request->get('alt');
            $data->text=$request->get('text');
            $data->pic=$path;
            $data->save();
            return redirect()->back()->with('success', 'Akun Berhasil Update');
        }
    }
    public function delete_slider($id)
    {
        $data=slider::find($id);
        Storage::delete($data->pic);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
}
