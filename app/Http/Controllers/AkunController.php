<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\karcab;
use App\karagen;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class AkunController extends Controller
{
    public function api(Request $request)
    {
        if ($request->has('q')) {
            $cari=$request->q;
            $data=User::where('name','LIKE','%'.$cari.'%')->get();
            // $data=User::where('role_id','!=','1')->where('name','LIKE','%'.$cari.'%')->get();
            return response()->json($data);
        }
    }
    public function data_akun()
    {
        return view('akun.data_akun');
    }
    public function json_akun()
    {
        $data=User::with(['role'])->get();
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-sm btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></button>
                        <!-- Modal-->
                        <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                
                                <div class="modal-body">
                                <form class="form-horizontal" action="'.route('akun.update_akun',$user->id).'" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="'.csrf_token().'">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label><b>Nama Lengkap*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" value="'.$user->name.'">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Username*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="text" class="form-control" name="username" placeholder="Username" required value="'.$user->username.'">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Kontak*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="number" class="form-control" name="hp" placeholder="Kontak" required value="'.$user->hp.'">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Ganti Password*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="text" class="form-control" name="pass" placeholder="Diisi jika ingin diganti"">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Foto Profil*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="file" class="form-control" name="pic">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12 pt-2">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                        <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-sm btn-warning"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        <!-- Modal-->
                        <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                
                                <div class="modal-body">
                                    <h4>
                                    Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                    <a href="'.route('akun.delete_akun',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                                    
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                        ';
                    })
                    ->addColumn('pic', function ($user) {
                        $pic=url('/').Storage::url($user->pic);
                        return'
                        <img src="'.$pic.'" alt="" width="100">
                        ';
                    })
                    ->addColumn('role', function ($user) {
                       if($user->role_id==1){
                        return '<span class="badge bg-danger">'.$user->role->name.'</span>';
                       }else if($user->role_id==2){
                        return '<span class="badge bg-warning">'.$user->role->name.'</span>';
                       }else if($user->role_id==3){
                        return '<span class="badge bg-success">'.$user->role->name.'</span>';
                       }else if($user->role_id==4){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==5){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==6){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==7){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==8){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==9){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==10){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==11){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==12){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }else if($user->role_id==13){
                        return '<span class="badge bg-info">'.$user->role->name.'</span>';
                       }
                    })
                    ->escapeColumns([])
                    ->make(true);
    }

    public function input_akun(Request $request)
    {
        $no_hp=$request->get('hp');
        $no_hp=str_replace(' ', '', $no_hp);
        $no_hp=str_replace('-', '', $no_hp);
        $no_hp=str_replace('+', '', $no_hp);
        $cekno_hp=substr($no_hp,0,1);
        if($cekno_hp==0){
            $no_hp=substr_replace($no_hp,"62",0,1);
        }else if($cekno_hp==8){
            $no_hp=substr_replace($no_hp,"628",0,1);
        }else if($cekno_hp==6){
        $no_hp=$no_hp;
        }
        $jumlah_no_hp=User::where('hp','=',$no_hp)->count();
        if ($jumlah_no_hp!=0) {
            return redirect()->back()->with('gagal', 'Nomor Hp Sudah Terdaftar');
        }else{
        $request->validate([
            'name'=>['required'],
            'username'=>['required'],
            'password'=>['required'],
            'pic'=>'belum',
            'jk'=>['required'],
            'hp'=>['required'],
            ]);
            $password=$request->get('password');
            $user_id = User::insertGetId([
                'name' => $request->get('name'),
                'username' => $request->get('username'), 
                'jk' => $request->get('jk'), 
                'hp' => $request->get('hp'), 
                'password' => Hash::make($password),
                'role_id' => $request->get('role'), 
                'pic' => "belum", 
                'status' => "NON-AKTIF", 
        ]);
    }
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function input_akun_cabang(Request $request,$id)
    {
        $no_hp=$request->get('hp');
        $no_hp=str_replace(' ', '', $no_hp);
        $no_hp=str_replace('-', '', $no_hp);
        $no_hp=str_replace('+', '', $no_hp);
        $cekno_hp=substr($no_hp,0,1);
        if($cekno_hp==0){
            $no_hp=substr_replace($no_hp,"62",0,1);
        }else if($cekno_hp==8){
            $no_hp=substr_replace($no_hp,"628",0,1);
        }else if($cekno_hp==6){
        $no_hp=$no_hp;
        }
        $jumlah_no_hp=User::where('hp','=',$no_hp)->count();
        if ($jumlah_no_hp!=0) {
            return redirect()->back()->with('gagal', 'Nomor Hp Sudah Terdaftar');
        }else{
        $request->validate([
            'name'=>['required'],
            'username'=>['required'],
            'password'=>['required'],
            'pic'=>'belum',
            'jk'=>['required'],
            'hp'=>['required'],
            ]);
            $password=$request->get('password');
            $user_id = User::insertGetId([
                'name' => $request->get('name'),
                'username' => $request->get('username'), 
                'jk' => $request->get('jk'), 
                'hp' => $request->get('hp'), 
                'password' => Hash::make($password),
                'role_id' => $request->get('role'), 
                'pic' => "belum", 
                'status' => "NON-AKTIF", 
        ]);
        karcab::create([
            'user_id'=>$user_id, 
            'cabang_id'=>$id, 
            ]);

    }
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function input_akun_agen(Request $request,$id)
    {
        $no_hp=$request->get('hp');
        $no_hp=str_replace(' ', '', $no_hp);
        $no_hp=str_replace('-', '', $no_hp);
        $no_hp=str_replace('+', '', $no_hp);
        $cekno_hp=substr($no_hp,0,1);
        if($cekno_hp==0){
            $no_hp=substr_replace($no_hp,"62",0,1);
        }else if($cekno_hp==8){
            $no_hp=substr_replace($no_hp,"628",0,1);
        }else if($cekno_hp==6){
        $no_hp=$no_hp;
        }
        $jumlah_no_hp=User::where('hp','=',$no_hp)->count();
        if ($jumlah_no_hp!=0) {
            return redirect()->back()->with('gagal', 'Nomor Hp Sudah Terdaftar');
        }else{
        $request->validate([
            'name'=>['required'],
            'username'=>['required'],
            'password'=>['required'],
            'pic'=>'belum',
            'jk'=>['required'],
            'hp'=>['required'],
            ]);
            $password=$request->get('password');
            $user_id = User::insertGetId([
                'name' => $request->get('name'),
                'username' => $request->get('username'), 
                'jk' => $request->get('jk'), 
                'hp' => $request->get('hp'), 
                'password' => Hash::make($password),
                'role_id' => $request->get('role'), 
                'pic' => "belum", 
                'status' => "NON-AKTIF", 
        ]);
        karagen::create([
            'user_id'=>$user_id, 
            'agen_id'=>$id, 
        ]);
    }
    return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function input_karyawan_cabang(Request $request)
    {
        $user=Auth::User()->id;
        $cabang=karcab::where('user_id','=',$user)->first();
        $no_hp=$request->get('hp');
        $no_hp=str_replace(' ', '', $no_hp);
        $no_hp=str_replace('-', '', $no_hp);
        $no_hp=str_replace('+', '', $no_hp);
        $cekno_hp=substr($no_hp,0,1);
        if($cekno_hp==0){
            $no_hp=substr_replace($no_hp,"62",0,1);
        }else if($cekno_hp==8){
            $no_hp=substr_replace($no_hp,"628",0,1);
        }else if($cekno_hp==6){
        $no_hp=$no_hp;
        }
        $jumlah_no_hp=User::where('hp','=',$no_hp)->count();
        if ($jumlah_no_hp!=0) {
            return redirect()->back()->with('gagal', 'Nomor Hp Sudah Terdaftar');
        }else{
        $request->validate([
            'name'=>['required'],
            'username'=>['required'],
            'password'=>['required'],
            'pic'=>'belum',
            'jk'=>['required'],
            'hp'=>['required'],
            ]);
            $password=$request->get('password');
            $user_id = User::insertGetId([
                'name' => $request->get('name'),
                'username' => $request->get('username'), 
                'jk' => $request->get('jk'), 
                'hp' => $request->get('hp'), 
                'password' => Hash::make($password),
                'role_id' => $request->get('role'), 
                'pic' => "belum", 
                'status' => "NON-AKTIF", 
        ]);
        karcab::create([
            'user_id'=>$user_id, 
            'cabang_id'=>$cabang->cabang_id, 
            ]);

    }
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function input_karyawan_agen(Request $request)
    {
        $user=Auth::User()->id;
        $agen=karagen::where('user_id','=',$user)->first();
        $no_hp=$request->get('hp');
        $no_hp=str_replace(' ', '', $no_hp);
        $no_hp=str_replace('-', '', $no_hp);
        $no_hp=str_replace('+', '', $no_hp);
        $cekno_hp=substr($no_hp,0,1);
        if($cekno_hp==0){
            $no_hp=substr_replace($no_hp,"62",0,1);
        }else if($cekno_hp==8){
            $no_hp=substr_replace($no_hp,"628",0,1);
        }else if($cekno_hp==6){
        $no_hp=$no_hp;
        }
        $jumlah_no_hp=User::where('hp','=',$no_hp)->count();
        if ($jumlah_no_hp!=0) {
            return redirect()->back()->with('gagal', 'Nomor Hp Sudah Terdaftar');
        }else{
        $request->validate([
            'name'=>['required'],
            'username'=>['required'],
            'password'=>['required'],
            'pic'=>'belum',
            'jk'=>['required'],
            'hp'=>['required'],
            ]);
            $password=$request->get('password');
            $user_id = User::insertGetId([
                'name' => $request->get('name'),
                'username' => $request->get('username'), 
                'jk' => $request->get('jk'), 
                'hp' => $request->get('hp'), 
                'password' => Hash::make($password),
                'role_id' => $request->get('role'), 
                'pic' => "belum", 
                'status' => "NON-AKTIF", 
        ]);
        karagen::create([
            'user_id'=>$user_id, 
            'agen_id'=>$agen->agen_id, 
            ]);

    }
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function update_akun(Request $request, $id)
    {
            $foto = $request->file('pic');
            $pass = $request->get('pass');
            if ($foto==null) {
                $data=User::find($id);
                $data->name=$request->get('name');
                $data->username=$request->get('username');
                $data->hp=$request->get('hp');
                $pic=$data->pic;
                $data->save();
                if ($pass!=null) {
                    $data=User::find($id);
                    $data->password=$pass;
                    $data->save();
                }
                return redirect()->back()->with('success', 'Akun Berhasil Update');
            }else{
                $path = $foto->store('public/profil');
                $data=User::find($id);
                $data->name=$request->get('name');
                $data->username=$request->get('username');
                $data->hp=$request->get('hp');
                $data->pic=$path;
                $data->save();
                if ($pass!=null) {
                    $data=User::find($id);
                    $data->password=$pass;
                    $data->save();
                }
                return redirect()->back()->with('success', 'Akun Berhasil Update');
            }
    }
    public function delete_akun($id)
    {
        $data=User::find($id);
            Storage::delete($data->pic);
            $data->delete();
            return redirect()->back()->with('success', 'Berhasil dihapus');
    }
    
}
