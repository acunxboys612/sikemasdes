<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
class BerandaController extends Controller
{
   
    public function master()
    {
        return view('beranda.master');
    }
    public function ceo()
    {
        return view('beranda.ceo');
    }
    public function keuangan()
    {
        $all=barang::where('status_barang','!=','Cancel')->count();
        $data_lunas=barang::where('status_pembayaran','=','Lunas')->count();
        $cod=barang::where('status_pembayaran','!=','Lunas')->count();
        $invoice=barang::where('status_pembayaran','!=','Lunas')->count();
        $cash=barang::where('status_pembayaran','!=','Lunas')->count();
        $diterima=barang::where('status_barang','=','Diterima')->count();
        $proses=barang::where('status_barang','!=','Diterima')->count();
        $omset=barang::sum('total_harga');
        $hpp=barang::sum('biaya_all_in');
        $profit=$omset-$hpp;
        $lunas=barang::where('status_pembayaran','=','Lunas')->sum('harga_jual_kg');
        $pending=barang::where('status_pembayaran','=','Pending')->sum('harga_jual_kg');
        $data_cb=cabang::with(['kas_in','kas_out','barang'=>function ($query)
        {
            return $query->with(['barang'])->get();
        }])->withCount('barang')->get();
        $data_ag=agen::with(['kas_in','kas_out','barang'=>function ($query)
        {
            return $query->with(['barang'])->get();
        }])->withCount('barang')->get();
        // dd($data_cb->toArray());
        return view('beranda.master',compact('all','data_lunas','cod','invoice','cash','diterima','proses','omset','lunas','pending','profit','data_cb','data_ag'));
    }
    public function admin()
    {
        $all=barang::where('status_barang','!=','Cancel')->count();
        $diterima=barang::where('status_barang','=','Diterima')->count();
        $proses=barang::where('status_barang','!=','Diterima')->count();
        return view('beranda.admin',compact('all','diterima','proses'));
    }
    public function keucab()
    {
        $user=Auth::User()->id;
        $cabang=karcab::where('user_id','=',$user)->first();

        $all=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->count();

        $data_lunas = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','=','LUNAS');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $cod = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('cabang_id','=',$cabang->cabang_id)->count();
        
        $invoice = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('cabang_id','=',$cabang->cabang_id)->count();
        
        $cash = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $diterima = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_barang','=','Diterima');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $proses = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_barang','!=','Diterima');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $data=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->get();
        $omset=0;
        foreach ($data as $datas) {
            $omset+=$datas->barang->harga_jual;
        }
        $hpp=0;
        foreach ($data as $datas) {
            $hpp+=$datas->barang->hpp;
        }
        $profit=$omset-$hpp;
        $bayar=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->get();
        $lunas=0;
        $pending=0;
        foreach ($bayar as $bayars) {
            if ($bayars->barang->status_pembayaran=="LUNAS") {
                $lunas+=$bayars->barang->harga_jual;
            }elseif ($bayars->barang->status_pembayaran=="Pending") {
                $pending+=$bayars->barang->harga_jual;
            }
        }
        return view('beranda.keucab',compact('all','data_lunas','cod','invoice','cash','diterima','proses','omset','profit','pending','lunas'));
    }
    public function adcab()
    {
        $user=Auth::User()->id;
        $cabang=karcab::where('user_id','=',$user)->first();

        $all=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->count();
        $diterima = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_barang','=','Diterima');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $proses = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_barang','!=','Diterima');
        })->where('cabang_id','=',$cabang->cabang_id)->count();
        return view('beranda.adcab',compact('all','proses','diterima'));
    }
    public function keuagen()
    {
        $user=Auth::User()->id;
        $agen=karagen::where('user_id','=',$user)->first();

        $all=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->count();

        $data_lunas = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','=','LUNAS');
        })->where('agen_id','=',$agen->agen_id)->count();

        $cod = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('agen_id','=',$agen->agen_id)->count();
        
        $invoice = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('agen_id','=',$agen->agen_id)->count();
        
        $cash = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('agen_id','=',$agen->agen_id)->count();

        $diterima = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_barang','=','Diterima');
        })->where('agen_id','=',$agen->agen_id)->count();

        $proses = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_barang','!=','Diterima');
        })->where('agen_id','=',$agen->agen_id)->count();

        $data=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->get();
        $omset=0;
        foreach ($data as $datas) {
            $omset+=$datas->barang->harga_jual;
        }
        $hpp=0;
        foreach ($data as $datas) {
            $hpp+=$datas->barang->hpp;
        }
        $profit=$omset-$hpp;
        $bayar=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->get();
        $lunas=0;
        $pending=0;
        foreach ($bayar as $bayars) {
            if ($bayars->barang->status_pembayaran=="LUNAS") {
                $lunas+=$bayars->barang->harga_jual;
            }elseif ($bayars->barang->status_pembayaran=="Pending") {
                $pending+=$bayars->barang->harga_jual;
            }
        }
        return view('beranda.keuagen',compact('all','data_lunas','cod','invoice','cash','diterima','proses','omset','profit','pending','lunas'));
    }
    public function adagen()
    {
        $user=Auth::User()->id;
        $agen=karagen::where('user_id','=',$user)->first();

        $all=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->count();
        $diterima = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_barang','=','Diterima');
        })->where('agen_id','=',$agen->agen_id)->count();

        $proses = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_barang','!=','Diterima');
        })->where('agen_id','=',$agen->agen_id)->count();
        return view('beranda.adagen',compact('all','proses','diterima'));
    }
    public function karyawan()
    {
        return view('beranda.karyawan');
    }
    public function karcab()
    {
        $user=Auth::User()->id;
        $cabang=karcab::where('user_id','=',$user)->first();

        $all=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->count();
        $diterima = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_barang','=','Diterima');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $proses = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_barang','!=','Diterima');
        })->where('cabang_id','=',$cabang->cabang_id)->count();
        return view('beranda.karcab',compact('all','proses','diterima'));
    }
    public function kargen()
    {
        $user=Auth::User()->id;
        $agen=karagen::where('user_id','=',$user)->first();

        $all=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->count();
        $diterima = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_barang','=','Diterima');
        })->where('agen_id','=',$agen->agen_id)->count();

        $proses = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_barang','!=','Diterima');
        })->where('agen_id','=',$agen->agen_id)->count();
        return view('beranda.kargen',compact('all','proses','diterima'));
    }
    public function kcabang()
    {
        $user=Auth::User()->id;
        $cabang=karcab::where('user_id','=',$user)->first();

        $barang=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->get();

        $all=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->count();

        $data_lunas = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','=','LUNAS');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $cod = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('cabang_id','=',$cabang->cabang_id)->count();
        
        $invoice = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('cabang_id','=',$cabang->cabang_id)->count();
        
        $cash = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $diterima = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_barang','=','Diterima');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $proses = barang_cabang::whereHas('barang', function ($query) {
            $query->where('status_barang','!=','Diterima');
        })->where('cabang_id','=',$cabang->cabang_id)->count();

        $data=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->get();
        $omset=0;
        foreach ($data as $datas) {
            $omset+=$datas->barang->harga_jual;
        }
        $hpp=0;
        foreach ($data as $datas) {
            $hpp+=$datas->barang->hpp;
        }
        $profit=$omset-$hpp;
        $bayar=barang_cabang::with(['barang'])->where('cabang_id','=',$cabang->cabang_id)->get();
        $lunas=0;
        $pending=0;
        foreach ($bayar as $bayars) {
            if ($bayars->barang->status_pembayaran=="LUNAS") {
                $lunas+=$bayars->barang->harga_jual;
            }elseif ($bayars->barang->status_pembayaran=="Pending") {
                $pending+=$bayars->barang->harga_jual;
            }
        }

        return view('beranda.kcabang',compact('all','data_lunas','cod','invoice','cash','diterima','proses','omset','profit','pending','lunas'));
    }
    public function kagen()
    {
        $user=Auth::User()->id;
        $agen=karagen::where('user_id','=',$user)->first();

        $barang=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->get();

        $all=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->count();

        $data_lunas = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','=','LUNAS');
        })->where('agen_id','=',$agen->agen_id)->count();

        $cod = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('agen_id','=',$agen->agen_id)->count();
        
        $invoice = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('agen_id','=',$agen->agen_id)->count();
        
        $cash = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_pembayaran','!=','LUNAS');
        })->where('agen_id','=',$agen->agen_id)->count();

        $diterima = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_barang','=','Diterima');
        })->where('agen_id','=',$agen->agen_id)->count();

        $proses = barang_agen::whereHas('barang', function ($query) {
            $query->where('status_barang','!=','Diterima');
        })->where('agen_id','=',$agen->agen_id)->count();

        $data=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->get();
        $omset=0;
        foreach ($data as $datas) {
            $omset+=$datas->barang->harga_jual;
        }
        $hpp=0;
        foreach ($data as $datas) {
            $hpp+=$datas->barang->hpp;
        }
        $profit=$omset-$hpp;
        $bayar=barang_agen::with(['barang'])->where('agen_id','=',$agen->agen_id)->get();
        $lunas=0;
        $pending=0;
        foreach ($bayar as $bayars) {
            if ($bayars->barang->status_pembayaran=="LUNAS") {
                $lunas+=$bayars->barang->harga_jual;
            }elseif ($bayars->barang->status_pembayaran=="Pending") {
                $pending+=$bayars->barang->harga_jual;
            }
        }

        return view('beranda.kagen',compact('all','data_lunas','cod','invoice','cash','diterima','proses','omset','profit','pending','lunas'));
    }
    public function enc($id)
    {
        $a=Hash::make($id);
        return $a;
    }
    
}
