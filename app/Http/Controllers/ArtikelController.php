<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\instansi;
use App\post;
use App\post_kategori;
use App\post_kategori_log;
use App\post_faq;
use App\post_seo;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class ArtikelController extends Controller
{
    public function form_artikel()
    {
        $kategori=post_kategori::all();
        return view('artikel.form_artikel',compact('kategori'));
    }
    public function form_update_post($id)
    {
        $kategori=post_kategori::all();
        // $tag=post_tag::all();
        $data=post::with(['post_kategori_log','post_seo','post_faq'])->find($id);
        //dd($data->toArray());
        return view('artikel.form_update_post',compact('kategori','data'));
    }
    public function data_artikel_all()
    {
        //
        return view('artikel.data_artikel_all');
    }
    public function json_artikel_all()
    {
        //
        $user_id=Auth::User()->id;
        $data=post::with(['user'])->get();
        // dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <a href="'.route('artikel.form_update_post',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-info" target="_blank"><i class="glyphicon glyphicon-edit"></i>Upadate</a>
                        <a href="'.route('artikel.delete_post',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Hapus</a>
                        <a href="'.route('artikel.data_faq',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-warning">FAQ</a>
                        ';
                    }) 
                    ->addColumn('img', function ($user) {
                        $ub=$user->img;
                        $url=url('/').Storage::url($user->img);
                            return '
                            <img src="'.$url.'" width="200px"  alt="img">
                            ';
                    })
                    ->addColumn('share', function ($user) {
                        $url=url('/').Storage::url($user->img);
                            return '
                            <a href="http://pinterest.com/pin/create/button/?url='.route('post',$user->slug).'&media='.$url.'&name='.$user->judul.'&description='.$user->judul.'" class="btn btn-xs btn-danger" target="_blank"><i class="fab fa-pinterest-p"></i></a>
                            ';
                    })->addColumn('faq', function ($user) {
                        if ($user->post_faq->count()==0) {
                            return 'FAQ KOSONG';
                        }else{
                            return $user->post_faq->count();
                        }
                     })
                     ->addColumn('user_id', function ($user) {
                        return $user->user->name;
                     })
                    ->escapeColumns([])
                    ->make(true);
    }

    public function input_post(Request $request)
    {
        $request->validate([
            'judul'=>['required'],
            'deskripsi'=>['required'],
            'isi'=>['required'],
            'img'=>['required'],
            'alt'=>['required'],
            'status'=>['required'],
            'deskripsi'=>['required'],
            ]);
            $post_tag_id=$request->input('post_tag_id');
            $post_kategori_id=$request->input('post_kategori_id');
            $img = $request->file('img');
            $path_img = $img->store('public/post');
            $user=Auth::User()->id;

            $post_id = post::insertGetId([
                'judul' => $request->get('judul'), 
                'slug' => Str::slug($request->get('judul')), 
                'deskripsi' => $request->get('deskripsi'), 
                'isi' => $request->get('isi'), 
                'img' => $path_img, 
                'alt' => $request->get('alt'), 
                'status' => $request->get('status'), 
                'tag' => $request->get('tag'), 
                'user_id' =>$user, 
                'created_at' =>now(), 
                'updated_at' =>now(), 
            ]);
            post_seo::create([
                'title'=> $request->get('judul'),
                'deskripsi'=> $request->get('deskripsi'),
                'keyword'=> $request->get('tag'),
                'post_id'=> $post_id,
            ]);
           
            
            if ($post_kategori_id!=null) {
            foreach($post_kategori_id as $post_kategoris){
                post_kategori_log::create([
                    'post_id'=> $post_id,
                    'post_kategori_id'=> $post_kategoris,
                ]);
            }
        }
        return redirect()->route('artikel.form_update_post', ['id' => $post_id])->with('success', 'Berhasil Ditambahkan');
    }
    public function update_post(Request $request, $id)
    {
        
            $data=post::find($id);
            $my=Auth::User()->id;
            $my_role=Auth::User()->role_id;
            // dd($my_role);
            if ($request->hasFile('img')) {
                Storage::delete($data->img);
                $img = $request->file('img');
                $path_img = $img->store('public/post');
                $data->img = $path_img; 
            }
            $data->judul = $request->get('judul'); 
            $data->slug = Str::slug($request->get('judul')); 
            $data->deskripsi = $request->get('deskripsi'); 
            $data->isi = $request->get('isi'); 
            $data->tag = $request->get('tag'); 
            $data->alt = $request->get('alt'); 
            $data->status = $request->get('status'); 
            $data->save();
            $seo=post_seo::where('post_id','=',$id)->first();
            //dd($seo->toArray());
            $seo->title=$request->get('judul');
            $seo->deskripsi=$request->get('deskripsi');
            $seo->keyword=$request->get('tag');
            $seo->save();
           
            $post_tag_id=$request->input('post_tag_id');
            $post_kategori_id=$request->input('post_kategori_id');
            
            if ($post_kategori_id!=null) {
                $kategori=post_kategori_log::where('post_id','=',$id)->delete();
                foreach($post_kategori_id as $post_kategoris){
                    post_kategori_log::create([
                        'post_id'=> $id,
                        'post_kategori_id'=> $post_kategoris,
                    ]);
            }
        }
        return redirect()->back()->with('success', 'Berhasil Diperbaharui');

    }
    
    public function delete_post($id)
    {
        $data=post::find($id);
        Storage::delete($data->img);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil Dihapus');
    }

    public function data_post_kategori()
    {
        //
        return view('artikel.data_kategori');
    }
    public function json_post_kategori()
    {
        //
        $data=post_kategori::all();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#myModal'.$user->id.'" class="btn btn-primary">Update</button>
                        <!-- Modal-->
                        <div id="myModal'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="exampleModalLabel" class="modal-title">Data post_kategori</h4>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <p>Silahkan tambhakan .</p>
                                    <form action="'.route('artikel.update_post_kategori',$user->id).'" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="'.csrf_token().'">
                                    <div class="form-group">
                                        <label>Nama kategori *</label>
                                        <input type="text" name="judul" class="form-control" required value="'.$user->judul.'">
                                    </div>
                                    <div class="form-group">
                                    <label>Slug *</label>
                                        <input type="text" name="slug" class="form-control" required value="'.$user->slug.'">
                                    </div>
                                    <div class="form-group">
                                        <label>Deskripsi *</label>
                                        <input type="text" name="deskripsi" class="form-control" required value="'.$user->deskripsi.'">
                                    </div>
                                    <div class="form-group">       
                                        <input type="submit" value="Update" class="btn btn-primary">
                                    </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                        <a href="'.route('artikel.delete_post_kategori',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Hapus</a>
                        ';
                    })
                    ->escapeColumns([])
                    ->make(true);
    }

    public function input_post_kategori(Request $request)
    {
        $request->validate([
            'judul'=>['required'],
            'slug'=>['required'],
            'deskripsi'=>['required'],
            ]);
            
            post_kategori::create([
                'judul' => $request->get('judul'), 
                'slug' => $request->get('slug'), 
                'deskripsi' => $request->get('deskripsi'), 
            ]);
         
        return redirect()->back()->with('success', 'Berhasil Ditambahkan');
    }

    
    public function update_post_kategori(Request $request, $id)
    {
        $data=post_kategori::find($id);
        
            $data->judul = $request->get('judul'); 
            $data->slug = $request->get('slug'); 
            $data->deskripsi = $request->get('deskripsi'); 
            $data->save();
        
        return redirect()->back()->with('success', 'Berhasil Diperbaharui');

    }

    public function delete_post_kategori($id)
    {
        $data=post_kategori::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil Dihapus');
    }

    public function data_faq($id)
    {
        return view('artikel.data_faq',compact('id'));
    }
    public function json_faq($id)
    {
        $data=post_faq::where('post_id','=',$id)->get();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#myModal'.$user->id.'" class="btn btn-primary">Update</button>
                        <!-- Modal-->
                        <div id="myModal'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="exampleModalLabel" class="modal-title">Data post_kategori</h4>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <p>Silahkan tambhakan .</p>
                                    <form action="'.route('artikel.update_faq',$user->id).'" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="'.csrf_token().'">
                                    <div class="form-group">
                                        <label>Tanya *</label>
                                        <input type="text" name="tanya" class="form-control" required value="'.$user->tanya.'">
                                    </div>
                                    <div class="form-group">
                                    <label>Jawab *</label>
                                        <input type="text" name="jawab" class="form-control" required value="'.$user->jawab.'">
                                    </div>
                                    <div class="form-group">       
                                        <input type="submit" value="Update" class="btn btn-primary">
                                    </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                        <a href="'.route('artikel.delete_faq',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Hapus</a>
                        ';
                    })
                    ->escapeColumns([])
                    ->make(true);
    }

    public function input_faq(Request $request)
    {
        $request->validate([
            'tanya'=>['required'],
            'jawab'=>['required'],
            'post_id'=>['required'],
            ]);
            post_faq::create([
                'tanya'=> $request->get('tanya'),
                'jawab'=> $request->get('jawab'),
                'post_id'=> $request->get('post_id'),
            ]);
        return redirect()->back()->with('success', 'Berhasil Ditambahkan');
    }
    public function update_faq(Request $request, $id)
    {
        $data=post_faq::find($id);
        $data->tanya=$request->get('tanya');
        $data->jawab=$request->get('jawab');
        $data->save();
        return redirect()->back()->with('success', 'Berhasil Diupdate');
    }
    public function delete_faq($id)
    {
        $data=post_faq::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil Dihapus');
    }
}
