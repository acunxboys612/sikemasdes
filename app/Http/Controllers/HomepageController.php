<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\post;
use App\post_kategori;
use App\post_kategori_log;
use App\post_seo;
use App\post_tag;
use App\post_tag_log;
use App\post_faq;
use App\page_seo;
use App\page_tag;
use App\page_faq;
use App\slider;
use App\layanan;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class HomepageController extends Controller
{
    public function index()
    {
        $post1=post_kategori::with(['post_kategori_log'])
                                ->where('judul','=','Layanan')
                                ->paginate(6);
        $slider=slider::all();
        $market=User::where('role_id','=',2)->orderBy('id','desc')->paginate(4);
        $layan=layanan::all();
        $post=post::with(['post_faq','post_seo','user','post_kategori_log'])->orderBy('id','desc')->paginate(3);
        return view('home',compact('slider','market','layan','post','post1'));
    }
    public function blog(Request $request)
    {
        $cari=$request->get('q');
            //dd($cari);
            $post1=post::with(['post_faq','post_seo','user','post_kategori_log'])->inRandomOrder()->paginate(5);
            $kategori=post_kategori::all();
            if ($cari!=null) {
                $post=post::with(['post_faq','post_seo','user','post_kategori_log'])
                ->where('judul', 'like', '%' . $cari . '%')
                ->orWhere('slug', 'like', '%' . $cari . '%')
                ->orWhere('deskripsi', 'like', '%' . $cari . '%')
                ->orWhere('tag', 'like', '%' . $cari . '%')
                            ->orderBy('id','DESC')
                            ->paginate(8);
            }else{
                $post=post::with(['post_faq','post_seo','user','post_kategori_log'])->orderBy('id','desc')->paginate(6);
            }
        return view('blog',compact('post','post1','kategori'));
    }
    public function sejarah ()
     {
        return view('sejarah');
    }
    public function visimisi ()
     {
        return view('visimisi');
    }
    public function geografis ()
     {
        return view('geografis');
    }
    public function inovasi ()
     {
        return view('inovasi');
    }
    public function galeri ()
     {
        return view('galeri');
    }
    public function kontak ()
     {
        return view('kontak');
    }
    public function populer()
    {
        $post1=post::with(['post_faq','post_seo','user','post_kategori_log'])->paginate(4);
        dd($post1->toArray());
        return view('populer',compact('post1'));
    }
    public function kategori($slug)
    {
        $post1=post::with(['post_faq','post_seo','user','post_kategori_log'])->inRandomOrder()->paginate(5);
        $kategori=post_kategori::all();
        $post=post_kategori::with(['post_kategori_log'])
                                ->where('slug','=',$slug)
                                ->first();
        if ($post!=null&&$post->post_kategori_log->toArray()!=[]) {
            $post=post_kategori::with(['post_kategori_log'=>function ($query)
                                    {
                                        return $query->with('post')->get();
                                    }])
                                ->where('slug','=',$slug)
                                ->get();
        return view('kategori',compact('post','post1','kategori'));
        }else{
            return view('404');
        }
        
    }
    public function tag($slug)
    {
        $kategori=post_kategori::all();
        $posting=post::all();
        $slug=str_replace('-',' ',$slug);
        $title=ucfirst(trans($slug));
        $post=post::where('tag','like',"%{$slug}%")->first();
        $post1=post::with(['post_faq','post_seo','user','post_kategori_log'])->inRandomOrder()->paginate(5);
        $faq=post_faq::where('post_id','=',$post->id)->get();
            $faq_count=post_faq::where('post_id','=',$post->id)->count();
            $tag = $post->tag;
            $tag = str_replace(array("\n", "\r"), '', $tag);;
            $arr_tag = explode (",",$tag);
            //$why=post::where('tag','=',$arr_tag)->get();
        return view('post_tag',compact('post','kategori','post1','posting','faq','faq_count','arr_tag'));
    }
    public function post($slug)
    {
        $kategori=post_kategori::all();
        $posting=post::all();
        $post=post::with(['post_faq','post_seo','user','post_kategori_log'])->where('slug','=',$slug)->count();
        if ($post!=0) {
            $post=post::with(['post_faq','post_seo','user','post_kategori_log'])->where('slug','=',$slug)->first();
            $post1=post::with(['post_faq','post_seo','user','post_kategori_log'])->inRandomOrder()->paginate(5);
            $faq=post_faq::where('post_id','=',$post->id)->get();
            $faq_count=post_faq::where('post_id','=',$post->id)->count();
            $tag = $post->tag;
            $tag = str_replace(array("\n", "\r"), '', $tag);;
            $arr_tag = explode (",",$tag);
            //dd($post->toArray());
            return view('post',compact('post','kategori','post1','posting','faq','faq_count','arr_tag'));
        }else{
            return view('404');
        }
    }
    public function notfound()
    {
        return view('404');
    }
    public function sitemap()
    {
        $html='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';
        $posts = post::select('slug','created_at','updated_at')->get();
        return response()->view('sitemap', [
            'posts' => $posts
        ])->header('Content-Type', 'text/xml');
        // dd($data->toArray());
    }
    public function login()
    {
        return view('login');
    }
    public function kirim_email(){

        $details = [
           'title' => 'Mail from websitepercobaan.com',
           'body' => 'This is for testing email using smtp'
           ];
          
           \Mail::to('acunxboys612@gmail.com')->send(new \App\Mail\MyTestMail($details));
          
           dd("Email sudah terkirim.");
           
        }
        public function postmap()
        {
            $html='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';
            $posts = post::select('slug','created_at','updated_at')->get();
            return response()->view('sitemap', [
                'posts' => $posts
            ])->header('Content-Type', 'text/xml');
            // dd($data->toArray());
        }
}
