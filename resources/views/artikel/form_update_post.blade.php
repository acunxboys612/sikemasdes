@extends('layouts.lb')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Update Post</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <form class="form-horizontal" action="{{route('artikel.update_post',$data->id)}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <input type="text" class="form-control" name="judul" placeholder="Judul Artikel" value="{{$data->judul}}" required>
                                                                <div class="form-control-position">
                                                                    <i class="fas fa-heading"></i>
                                                                </div>
                                                                @if ($errors->has('judul'))
                                                                    <span class="help-block text-danger">
                                                                        {{ $errors->first('judul') }}
                                                                    </span>
                                                                @endif
                                                        </fieldset>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <textarea class="form-control" name="isi" id="isi" rows="10" placeholder="Isi Artikel">{{$data->isi}}</textarea>
                                                                <div class="form-control-position">
                                                                    <i class="fa fa-pencil"></i>
                                                                </div>
                                                                @if ($errors->has('isi'))
                                                                    <span class="help-block text-danger">
                                                                        {{ $errors->first('isi') }}
                                                                    </span>
                                                                @endif
                                                        </fieldset>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                            <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5" placeholder="Deskripsi Artikel">{{$data->deskripsi}}</textarea>
                                                                <div class="form-control-position">
                                                                    <i class="fa fa-book"></i>
                                                                </div>
                                                                @if ($errors->has('deskripsi'))
                                                                    <span class="help-block text-danger">
                                                                        {{ $errors->first('deskripsi') }}
                                                                    </span>
                                                                @endif
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group position-relative has-icon-left">
                                                                    <input type="text" class="form-control" name="alt" placeholder="Alt gambar" value="{{$data->alt}}" required>
                                                                        <div class="form-control-position">
                                                                            <i class="fa fa-pencil"></i>
                                                                        </div>
                                                                        @if ($errors->has('alt'))
                                                                            <span class="help-block text-danger">
                                                                                {{ $errors->first('alt') }}
                                                                            </span>
                                                                        @endif
                                                                </fieldset>
                                                                <fieldset class="form-group position-relative has-icon-left">
                                                                    <div id="gambar">
                                                                    <img src="{{url('/').Storage::url($data->img)}}" class="img-thumbnail" alt="Responsive image" id="gambar">
                                                                    </div>
                                                                    <input type="file" class="form-control" name="img" id="img">
                                                                        <div class="form-control-position">
                                                                        <i class="fas fa-images"></i>
                                                                        </div>
                                                                        @if ($errors->has('img'))
                                                                            <span class="help-block text-danger">
                                                                                {{ $errors->first('img') }}
                                                                            </span>
                                                                        @endif
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <fieldset class="form-group position-relative has-icon-left">
                                                                    <select class="form-control" name="status">
                                                                        <option value="Publish">Publish</option>
                                                                        <option value="Draf">Draf</option>
                                                                    </select>
                                                                    <div class="form-control-position">
                                                                        <i class="fa fa-book"></i>
                                                                    </div>
                                                                    @if ($errors->has('status'))
                                                                        <span class="help-block text-danger">
                                                                            {{ $errors->first('status') }}
                                                                        </span>
                                                                    @endif
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <fieldset class="form-group position-relative has-icon-left">
                                                                    <textarea class="form-control" name="tag" id="tag" rows="5" placeholder="Tag dipisah tanda koma">{{$data->tag}}</textarea>
                                                                    <div class="form-control-position">
                                                                        <i class="fa fa-book"></i>
                                                                    </div>
                                                                    @if ($errors->has('tag'))
                                                                        <span class="help-block text-danger">
                                                                            {{ $errors->first('tag') }}
                                                                        </span>
                                                                    @endif
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <fieldset class="form-group position-relative has-icon-left">
                                                                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                                                        @foreach($kategori as $kategoris)
                                                                        <div class="custom-control custom-checkbox mr-sm-2">
                                                                            <input type="checkbox" value="{{$kategoris->id}}" class="custom-control-input" id="kategori{{$kategoris->id}}" name="post_kategori_id[]">
                                                                            <label class="custom-control-label" for="kategori{{$kategoris->id}}">{{$kategoris->judul}}</label>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <fieldset class="form-group position-relative has-icon-left">
                                                                    <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
      function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                $('#gambar').html('<img src="'+e.target.result+'" class="img-thumbnail" alt="Responsive image" id="gambar">');
                
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
            }

            $("#img").change(function() {
            readURL(this);
            });
  </script>
  <script>
      $('#isi').summernote({
        placeholder: 'AYO SEMANGAT BUAT ARTIKEL',
        tabsize: 2,
        height: 300
      });
    </script>
<script type="text/javascript">
  $(function() {
    $('#tags').tagsInput();
  });

  $(function() {
    $('#keyword').tagsInput();
  });
  </script>
@endpush