@extends('layouts.lb')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Data Faq</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="row" style="padding-bottom:10px;">
                                        <div class="col-md-3">
                                        <button type="button" data-toggle="modal" data-target="#tambah" class="btn btn-block btn-secondary">Tambah Faq</button>
                                        </div>
                                        <div id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                            <div role="document" class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-body">
                                                <form action="{{route('artikel.input_faq')}}" method="post">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label>Tanya *</label>
                                                        <input type="text" name="tanya" class="form-control" placeholder="Tanya" required>
                                                        <input type="hidden" name="post_id" class="form-control" value="{{$id}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Jawab *</label>
                                                        <input type="text" name="jawab" class="form-control" placeholder="Jawab" required>
                                                    </div>
                                                    <div class="form-group">       
                                                        <input type="submit" value="Tambah" class="btn btn-block btn-secondary">
                                                    </div>
                                                </form> 
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                                </div>
                                                
                                                </div>
                                            </div>
                                            <!-- end of modal -->
                                        </div>
                                    </div>
                                            <div class="container table-responsive">                        
                                                <table class="table table-striped table-bordered" id="data-pelanggan">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>No</th>
                                                            <th>Tanya</th>
                                                            <th>Jawab</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
//fucktion awal
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    '<tr>'+
            '<td>Aksi:</td>'+
            '<td>'+d.action+'</td>'+
        '</tr>'+
    '</table>';
}
//end function
$(document).ready(function() {
    var table = $('#data-pelanggan').DataTable({
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        ajax: "{{route('artikel.json_faq',$id)}}",
        columns: [
            {"className": 'details-control',"orderable": false,"data": null,"defaultContent": ''},
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'tanya', name: 'tanya' },
            { data: 'jawab', name: 'jawab' },
        ],
    });
// Add event listener for opening and closing details
    $('#data-pelanggan tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
// Batas bawah
});
</script>
@endpush