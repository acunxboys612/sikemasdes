@extends('layouts.home.layout')
@section('content')
<!-- Page Header Start -->
<div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container text-center py-5">
            <h1 class="display-3 text-white mb-4 animated slideInDown">BERITA DESA</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb justify-content-center mb-0">
                    <li class="breadcrumb-item"><a href="{{route('homepage')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Berita</li>
                </ol>
            </nav>
        </div>
    </div>
    <!-- Page Header End -->


    <!-- Team Start -->
    <div class="container-xxl">
        <div class="container">
            <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
                <h1 class="display-5 mb-5">BERITA DESA</h1>
            </div>
            <div class="row g-5">
            @foreach($post as $data)
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="{{url('/').Storage::url($data->img)}}" alt="{{$data->alt}}">
                        <div class="card-body">
                          <h5 class="card-title text-center"><a href="{{route('post',$data->slug)}}">{{$data->judul}}</a></h5>
                          <p class="card-text text-center">{{$data->deskripsi}}</p>
                        </div>
                      </div>
                </div>
            </div>
            @endforeach
            {{ $post->links() }}
        </div>
    </div>
    <!-- Team End -->
@endsection
@push('scripts')
@endpush