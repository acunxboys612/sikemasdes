@extends('layouts.lb')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                 <!-- Enable - disable FixedHeader table -->
                 <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Masukan Karyawan Cabang</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="row" style="padding-bottom:10px;">
                                            <div class="col-md-12">
                                                <form action="{{route('kantor.set_kacab',$id)}}" method="post" class="needs-validation" novalidate>
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label><b>Pilih Karyawan*</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <select class="form-control" name="user_id" id="user" required></select>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 pt-2">
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="submit" value="TAMBAHKAN" class="btn btn-block btn-secondary">
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </form> 
                                            </div>
                                                
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
                 <!-- Enable - disable FixedHeader table -->
                 <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Karyawan {{$data->nama_cabang}}</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">                        
                                            <table class="table table-striped table-bordered" id="data-pelanggan">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Role</th>
                                                        <th>Nama Lengkap</th>
                                                        <th>Tools</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
    //fucktion awal
    function format ( d ) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
            '<tr>'+
                '<td>Aksi:</td>'+
                '<td>'+d.action+'</td>'+
            '</tr>'+
        '</table>';
    }
    //end function
    $(document).ready(function() {
        var table = $('#data-pelanggan').DataTable({
            processing: true,
            serverSide: true,
            scrollCollapse: true,
            ajax: "{{route('kantor.json_kacab',"+$id+")}}",
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex'},
                { data: 'role', name: 'role' },
                { data: 'user.name', name: 'user.name' },
                { data: 'action', name: 'action' },
            ],
        });
    // Add event listener for opening and closing details
        $('#data-pelanggan tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
     
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        });
    // Batas bawah
    });
    </script>
<script type="text/javascript">
    $('#user').select2({
      placeholder: 'Karyawan',
      ajax: {
        url: '{{route("akun.api")}}',
        dataType: 'json',
        delay: 250,
        data: function (params) {
                      return {
                          q: $.trim(params.term)
                      };
                  },
        processResults: function (data) {
          return {
            results:  $.map(data, function (item) {
              return {
                text: item.name,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });
  
  </script>
@endpush