@extends('layouts.lb')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Data Pengguna</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                <div class="row" style="padding-bottom:10px;">
                                    <div class="col-md-3">
                                    <button type="button" data-toggle="modal" data-target="#tambah" class="btn btn-block btn-secondary">Tambah Pengguna</button>
                                    </div>
                                    <div id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                        <div role="document" class="modal-dialog">
                                            <div class="modal-content">
                                            <div class="modal-body">
                                            <form action="{{route('akun.input_akun')}}" method="post" class="needs-validation" novalidate>
                                                @csrf
                                                <div class="form-group">
                                                    <label>Nama Lengkap </label>
                                                    <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Username </label>
                                                    <input type="text" name="username" class="form-control" placeholder="Username" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Password </label>
                                                    <input type="text" name="password" class="form-control" placeholder="Password" required>
                                                </div>
                                                <div class="form-group">
                                                <label>Jenis Kelamin </label>
                                                <select class="form-control" name="jk"  required>
                                                <option value="L" >Laki-Laki</option>
                                                <option value="P" >Perempuan</option>
                                                </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Role </label>
                                                    <select class="form-control" name="role"  required>
                                                    <option value="1" >Master</option>
                                                    <option value="4" >Admin</option>
                                                    <option value="3" >Keuangan</option>
                                                    </select>
                                                    </div>
                                                <div class="form-group">
                                                    <label>Kontak </label>
                                                    <input type="number" name="hp" class="form-control" placeholder="Ex. 0878XXXXXXXX" required>
                                                </div>
                                                <div class="form-group">       
                                                    <input type="submit" value="Tambah" class="btn btn-block btn-secondary">
                                                </div>
                                            </form> 
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                            </div>
                                            
                                            </div>
                                        </div>
                                        <!-- end of modal -->
                                    </div>
                                </div>
                                            <div class="table-responsive">                        
                                                <table class="table table-striped table-bordered" id="data-pelanggan">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Role</th>
                                                            <th>Nama Lengkap</th>
                                                            <th>JK</th>
                                                            <th>Kontak</th>
                                                            <th>Foto Profil</th>
                                                            <th>Tools</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
//fucktion awal
function format ( d ) {
    // `d` is the original data object for the row
    return '';
}
//end function
$(document).ready(function() {
    var table = $('#data-pelanggan').DataTable({
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        ajax: "{{route('akun.json_akun')}}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'role', name: 'role' },
            { data: 'name', name: 'name' },
            { data: 'jk', name: 'jk' },
            { data: 'hp', name: 'hp' },
            { data: 'pic', name: 'pic' },
            { data: 'action', name: 'action' },
        ],
    });
// Add event listener for opening and closing details
    $('#data-pelanggan tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
// Batas bawah
});
</script>
@endpush