@extends('layouts.lb')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">PROFIL {{Auth::User()->name}}</i></h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="row" style="padding-bottom:10px;">
                                            <div class="col-lg-12 text-center mb-3">
                                            @if(Auth::User()->pic=="belum")
                                            <img src="{{asset('img/logo.png')}}"  alt="Responsive image" width="200" height="200">
                                            @else
                                            <img src="{{url('/').Storage::url(Auth::User()->pic)}}" style="border-radius: 50%;" alt="Responsive image" width="200" height="200">
                                            @endif
                                            </div>
                                            <div class="col-lg-12 text-center">
                                                <a href="{{route('profil.data_profil')}}" class="btn btn-secondary btn-block">EDIT PROFIL</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">DETAIL</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                <div class="row" style="padding-bottom:10px;">
                                    <div class="col-md-12">
                                    <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>{{Auth::User()->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jenis Kelamin</td>
                                                    <td>:</td>
                                                    <td>{{Auth::User()->jk}}</td>
                                                </tr>
                                                <tr>
                                                    <td >WhatsApp</td>
                                                    <td >:</td>
                                                    <td>{{Auth::User()->hp}}</td>
                                                </tr>
                                                @foreach ($data as $datas)
                                                <tr>
                                                    <td >Jabatan</td>
                                                    <td >:</td>
                                                    <td>{{($datas->role->name)}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
@endpush