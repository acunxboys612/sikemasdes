@extends('layouts.lb')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                    <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">EDIT FOTO</i></h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                <div class="row" style="padding-bottom:10px;">
                                    <div class="lg-12 col-md-12">
                                    <div class="row">
                                            <div class="col-lg-12 text-center">
                                            @if(Auth::User()->pic=="belum")
                                            <img src="{{asset('img/logo.png')}}" class="img-fluid" alt="Responsive image" width="200" height="300">
                                            @else
                                            <img src="{{url('/').Storage::url(Auth::User()->pic)}}" class="img-fluid" alt="Responsive image" width="300" height="400">
                                            @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="basic-login-inner">
                                                    <form action="{{route('profil.update_foto_profil')}}" method="post" enctype="multipart/form-data" class="text-left">
                                                            @csrf
                                                            <div class="form-group">
                                                                <label>Pilih File</label>
                                                                <input type="file" name="pic" class="form-control" required>
                                                            </div>
                                                            <div class="form-group">       
                                                            <input type="submit" value="Upload" class="btn btn-secondary btn-block">
                                                            </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">PROFIL</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                <div class="row" style="padding-bottom:10px;">
                                    <div class="col-md-12">
                                            <form action="{{route('profil.update_profil')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label>Nama Lengkap *</label>
                                                    <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" value="{{Auth::User()->name}}" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis Kelamin *</label>
                                                    <select class="form-control" name="jk" required>
                                                    <option value="L" {{ (Auth::User()->jk=="L") ? 'selected' : '' }}>Laki - Laki</option>
                                                    <option value="P" {{ (Auth::User()->jk=="P") ? 'selected' : '' }} >Perempuan</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>WhatsApp *</label>
                                                    <input type="text" name="hp" class="form-control" placeholder="Ex. 628XXXXXXXXXX" value="{{Auth::User()->hp}}" required>
                                                </div>
                                                <div class="form-group">       
                                                    <input type="submit" value="UPDATE" class="btn btn-block btn-secondary">
                                                </div>
                                            </form> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
@endpush