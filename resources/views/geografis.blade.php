@extends('layouts.home.layout')
@section('content')
    <!-- Page Header Start -->
    <div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container text-center py-5">
            <h1 class="display-3 text-white mb-4 animated slideInDown">GEOGRAFIS DESA</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb justify-content-center mb-0">
                    <li class="breadcrumb-item"><a href="{{route('homepage')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Profil</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Geografis Desa</li>
                </ol>
            </nav>
        </div>
    </div>
    <!-- Page Header End -->


    <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
                <p class="fs-5 fw-bold text-primary">Letak Geografis Desa</p>
                <h1 class="display-5 mb-5">GEOGRAFIS DESA</h1>
            </div>
            <div class="position-relative rounded overflow-hidden h-100">
                <!-- <iframe style="height:300px;width:100%;border:1;" frameborder="2" src="https://www.google.com/maps/embed/v1/place?q=Universitas+Trunojoyo+Madura,+Jalan+Raya+Telang,+Perumahan+Telang+Inda,+Telang,+Kabupaten+Bangkalan,+Jawa+Timur,+Indonesia&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"
                frameborder="2" allowfullscreen="" aria-hidden="false"
                tabindex="2"
                ></iframe> -->
                <iframe style="height:300px;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Tellang,+Kabupaten+Bangkalan,+Jawa+Timur,+Indonesia&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"
                frameborder="2" allowfullscreen="" aria-hidden="false"
                tabindex="2"
                ></iframe>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@endpush