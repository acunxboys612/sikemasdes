<script type="application/ld+json"> {
	"@context": "https://schema.org",
"@type": "CreativeWorkSeries",
    "name": "{{$post->judul}}",
    "aggregateRating": {
        "@type": "AggregateRating",
		"ratingValue": "5",
      	"bestRating": "5",
      	"worstRating": "1",
      	"ratingCount" : "25"
	},
	"@graph": [{
		"@type": ["Organization", "Place"],
		"@id": "https://hrdcargo.id/#organization",
		"name": "PT. Harddies Group Indonesia",
		"url": "https://hrdcargo.id/",
		"logo": {
			"@type": "ImageObject",
			"@id": "https://hrdcargo.id/#logo",
			"inLanguage": "id-ID",
			"url": "https://hrdcargo.id/img/logo.png",
			"width": 489,
			"height": 320,
			"caption": "PT. Harddies Group Indonesia"
		},
		"image": {
			"@id": "https://hrdcargo.id/#logo"
		},
		"location": {
			"@id": "https://hrdcargo.id/#local-place"
		},
		"address": {
			"@id": "https://hrdcargo.id/#local-place-address"
		},
		"email": "ltrexpress.info@gmail.com",
		"telephone": "+6282117050590",
		"areaServed": "Surabaya"
	}, {
		"@type": "WebSite",
		"@id": "https://hrdcargo.id/#website",
		"url": "https://hrdcargo.id/",
		"name": "{{$post->judul}}",
		"description": "{{$post->deskripsi}}",
		"publisher": {
			"@id": "https://hrdcargo.id/#organization"
		},
		"potentialAction": [{
			"@type": "SearchAction",
			"target": "https://hrdcargo.id/search/{search_term_string}",
			"query-input": "required name=search_term_string"
		}],
		"inLanguage": "id-ID"
	}, {
		"@type": "WebPage",
		"@id": "https://hrdcargo.id/#webpage",
		"url": "https://hrdcargo.id/",
		"name": "{{$post->judul}}",
		"isPartOf": {
			"@id": "https://hrdcargo.id/#website"
		},
		"about": {
			"@id": "https://hrdcargo.id/#organization"
		},
		"datePublished": "{{$post->created_at}}",
		"dateModified": "{{$post->updated_at}}",
		"description": "{{$post->deskripsi}}",
		"inLanguage": "id-ID",
		"potentialAction": [{
			"@type": "ReadAction",
			"target": ["https://hrdcargo.id/"]
		}]
	}, @if($faq->toArray()!=[]){"@type": "FAQPage","mainEntity": [@php($no=1)@foreach($faq as $faqs){
                    "@type": "Question",
                    "@id": "https://hrdcargo.id/#faq-question-{{$no}}",
                    "position": {{$no}},
                    "url": "{{url('/')}}/{{$post->slug}}",
                    "name": "{{$faqs->tanya}}",
                    "answerCount": 1,
                    "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "{{$faqs->jawab}}",
                        "inLanguage": "id-ID"
                    },
                    "inLanguage": "id-ID"
                } @if($no==$faq_count) @else , @endif @php($no++) @endforeach  ]}, @endif {
		
	}]
} 
</script>