      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top p-0">
        <a href="{{route('homepage')}}" class="navbar-brand d-flex align-items-center px-4 px-lg-5">
            <h1 class="m-0">SIKEMASDES</h1>
        </a>
        <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto p-4 p-lg-0">
                <a href="{{route('homepage')}}" class="nav-item nav-link {{ (request()->routeIs('homepage')) ? 'active' : '' }}">Beranda</a>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Profil</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="{{route('sejarah')}}" class="dropdown-item {{ (request()->routeIs('sejarah')) ? 'active' : '' }}">Sejarah Desa</a>
                        <a href="{{route('visimisi')}}" class="dropdown-item {{ (request()->routeIs('visimisi')) ? 'active' : '' }}">Visi & Misi Desa</a>
                        <a href="{{route('geografis')}}" class="dropdown-item {{ (request()->routeIs('geografis')) ? 'active' : '' }}">Geografis Desa</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pemerintahan</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="" class="dropdown-item">Struktur Desa</a>
                        <a href="" class="dropdown-item">BPD</a>
                        <a href="" class="dropdown-item">LKD</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Produktifitas</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="" class="dropdown-item">SDA</a>
                        <a href="" class="dropdown-item">SDM</a>
                        <a href="" class="dropdown-item">Produk Unggulan</a>
                    </div>
                </div>
                <a href="{{route('inovasi')}}" class="nav-item nav-link {{ (request()->routeIs('inovasi')) ? 'active' : '' }}">Inovasi</a>
                <a href="{{route('galeri')}}" class="nav-item nav-link {{ (request()->routeIs('galeri')) ? 'active' : '' }}">Galeri</a>
                <a href="{{route('blog')}}" class="nav-item nav-link {{ (request()->routeIs('blog')) ? 'active' : '' }}">Berita</a>
                <a href="{{route('kontak')}}" class="nav-item nav-link {{ (request()->routeIs('kontak')) ? 'active' : '' }}">Kontak</a>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->