<!DOCTYPE html>
<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="{{env('APP_DEC', 'default_value')}}">
    <meta name="keywords" content="{{env('APP_DEC', 'default_value')}}">
    <meta name="author" content="{{env('APP_AUTH', 'default_value')}}">
    <title>{{env('APP_NAME', 'default_value')}}</title>
    <link rel="apple-touch-icon" href="{{asset('img/logo.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/logo.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.css')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column   menu-collapsed blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
        @yield('content')
    <!-- END: Content-->

    @include('layouts.js')
    @include('alert')
    @stack('scripts')

</body>
<!-- END: Body-->

</html>