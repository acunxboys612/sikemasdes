<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header"><span>MENU</span><i class=" feather icon-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
            </li>
            <li class=" nav-item {{ (request()->routeIs('beranda.master')) ? 'active' : '' }}"><a href="{{route('beranda.master')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
            </li>
            <li class=" nav-item"><a href="#"><i class="fas fa-blog"></i><span class="menu-title" data-i18n="Dashboard">Artikel</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('artikel.data_artikel_all')) ? 'active' : '' }}"><a class="menu-item" href="{{route('artikel.data_artikel_all')}}" data-i18n="Master">All Post</a>
                    </li>
                    <li class="{{ (request()->routeIs('artikel.form_artikel')) ? 'active' : '' }}"><a class="menu-item" href="{{route('artikel.form_artikel')}}" data-i18n="Master">New Post</a>
                    </li>
                    <li class="{{ (request()->routeIs('artikel.data_post_kategori')) ? 'active' : '' }}"><a class="menu-item" href="{{route('artikel.data_post_kategori')}}" data-i18n="Master">Kategori</a>
                    </li>
                    <li class="{{ (request()->routeIs('slider.data_slider')) ? 'active' : '' }}"><a class="menu-item" href="{{route('slider.data_slider')}}" data-i18n="Master">Slider</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->