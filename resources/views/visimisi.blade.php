@extends('layouts.home.layout')
@section('content')
    <!-- Page Header Start -->
    <div class="container-fluid page-header py-5 mb-10 wow fadeIn" data-wow-delay="0.1s">
        <div class="container text-center py-5">
            <h1 class="display-3 text-white mb-4 animated slideInDown">VISI & MISI DESA</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb justify-content-center mb-0">
                    <li class="breadcrumb-item"><a href="{{route('homepage')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Profil</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Visi & Misi Desa</li>
                </ol>
            </nav>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
                <p class="fs-5 fw-bold text-primary">Visi & Misi</p>
                <h1 class="display-5 mb-5">VISI DESA</h1>
                <h1 class="display-5 mb-5">MISI DESA</h1>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@endpush