<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('roles')->insert([
            'name' => "Master",
            'description' => "Digunakan sebagai Role Master",
        ]);
        DB::table('users')->insert([
            'name'=>"Master", 
            'username'=>"master", 
            'password'=>'$2y$10$twugdXWbaKn0krvDlVY35.5O67ievL5LQf08Xecif3s/QxAUjpe5C',
            'pic'=>"belum",
            'hp'=>"6287754683648",
            'jk'=>"L",
            'status'=>"AKTIF",
            'role_id'=>1,
        ]);
    }
}
